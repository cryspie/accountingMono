﻿using System.ComponentModel.DataAnnotations;

namespace cryspie.DAL.Entities;

public class PhoneNumber:EntityBase
{
    //[Required(ErrorMessage = "Phone Number is required.")]
    //[RegularExpression(@"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$", ErrorMessage = "Invalid phone number.")]
    public string? Number { get; set; }
}