﻿using System.ComponentModel.DataAnnotations;

namespace cryspie.DAL.Entities;

public class Country:EntityBase
{
    public string Name { get; set; }
}