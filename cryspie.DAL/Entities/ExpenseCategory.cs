﻿namespace cryspie.DAL.Entities;

public class ExpenseCategory:EntityBase
{
    public string Name { get; set; }
    public string Color { get; set; }
}