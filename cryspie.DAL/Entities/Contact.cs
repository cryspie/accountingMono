﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cryspie.DAL.Entities
{
    public class Contact : EntityBase
    {
        public string Name { get; set; }
        public virtual Address Address { get; set; }
        public virtual PhoneNumber PhoneNumber { get; set; }
    }
}
