﻿namespace cryspie.DAL.Entities;

public class Expense:EntityBase
{
    public double Cost { get; set; }
    public string? Comment { get; set; }
    public virtual ExpenseCategory Category { get; set; }
    public virtual Person Person { get; set; }
}