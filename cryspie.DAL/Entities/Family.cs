﻿using System.Collections.Generic;

namespace cryspie.DAL.Entities;

public class Family:EntityBase
{
    public string Name { get; set; }
    public virtual List<Person> Person { get; set; }
}