﻿using cryspie.DAL.Entities;

namespace cryspie.DAL.Entities;

public class Address:EntityBase
{
    public string StreetName { get; set; }
    public virtual Country Country { get; set; }
    public virtual City City { get; set; }
    public long PostCode { get; set; }
    public string Comments { get; set; }
}