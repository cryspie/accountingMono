﻿using cryspie.DAL.Entities;

namespace cryspie.DAL.Entities;

public class City:EntityBase
{
    public string Name { get; set; }
}