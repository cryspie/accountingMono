﻿using System.Collections.Generic;
using cryspie.DAL.Entities;

namespace cryspie.DAL.Entities;

public class Person : EntityBase
{
    public string Name { get; set; }
    public string? FirstName { get; set; }
    public string? LastName { get; set; }
    public string Email { get; set; }
    public string Login { get; set; }
    public string Password { get; set; }
    public virtual KinShip KinShip { get; set; }
    public virtual Contact Contact { get; set; }
}