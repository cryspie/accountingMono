using cryspie.DAL.Repositories.DbRepositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.DAL.Extensions;

public static class ServiceExtensions
{
    public static void ConfigurationContextDAL(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<AppDbContext>(option =>
        {
            option
                .UseLazyLoadingProxies()
                .UseNpgsql(configuration.GetSection("ConnectionStrings")["Postgresql"]);
        });
    }

    public static void ConfigurationRepositories(this IServiceCollection services)
    {
        services.AddScoped<IPersonRepository, PersonRepository>();
        services.AddScoped<IAddresRepository, AddresRepository>();
        services.AddScoped<ICityRepository, CityRepository>();
        services.AddScoped<ICountryRepository, CountryRepository>();
        services.AddScoped<IExpenseRepository, ExpenseRepository>();
        services.AddScoped<IExpenseCategoryRepository, ExpenseCategoryRepository>();
        services.AddScoped<IFamilyRepository, FamilyRepository>();
        services.AddScoped<IKinShipRepository, KinShipRepository>();
        services.AddScoped<IPhoneNumberRepository, PhoneNumberRepository>();
    }
}