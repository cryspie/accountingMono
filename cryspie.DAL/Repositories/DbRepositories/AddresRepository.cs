﻿using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.DAL.Repositories.DbRepositories;

public class AddresRepository : BaseRepository<Address>, IAddresRepository
{
    public AddresRepository(AppDbContext context) : base(context)
    {
    }
}