﻿using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.DAL.Repositories.DbRepositories;

public class KinShipRepository : BaseRepository<KinShip>, IKinShipRepository
{
    public KinShipRepository(AppDbContext context) : base(context)
    {
    }
}