using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.DAL.Repositories.DbRepositories;

public class PersonRepository:BaseRepository<Person>,IPersonRepository
{
    public PersonRepository(AppDbContext context) : base(context)
    {
    }
    public Person GetByLogin(string login)
    {
        return _dbSet.FirstOrDefault(f => f.Login == login);
    }

    //public double GetCurrentByCircuit(double power, double voltage, double cosF)
    //{
    //    var result = power / (cosF * voltage);

    //    return result;
    //}
}