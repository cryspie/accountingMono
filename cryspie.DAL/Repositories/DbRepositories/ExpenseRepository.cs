﻿using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.DAL.Repositories.DbRepositories;

public class ExpenseRepository : BaseRepository<Expense>, IExpenseRepository
{
    public ExpenseRepository(AppDbContext context) : base(context)
    {
    }

    public IEnumerable<Expense> GetAllByPersonIds(IEnumerable<long> personIds)
    {
       return  _dbSet.Where(x =>personIds.All(y=>y==x.Person.Id));
    }

    public IEnumerable<Expense> GetAllByPersonId(long personId)
    {
        return _dbSet.Where(x => x.Person.Id == personId);
    }
}