﻿using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.DAL.Repositories.DbRepositories;

public class CountryRepository : BaseRepository<Country>, ICountryRepository
{
    public CountryRepository(AppDbContext context) : base(context)
    {
    }
}