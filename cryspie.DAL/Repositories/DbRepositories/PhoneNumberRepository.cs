﻿using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.DAL.Repositories.DbRepositories;

public class PhoneNumberRepository : BaseRepository<PhoneNumber>, IPhoneNumberRepository
{
    public PhoneNumberRepository(AppDbContext context) : base(context)
    {
    }
}