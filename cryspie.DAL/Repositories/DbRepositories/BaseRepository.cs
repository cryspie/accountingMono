﻿using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace cryspie.DAL.Repositories.DbRepositories;

public class BaseRepository<T> : IBaseRepository<T>
    where T : EntityBase
{
    private AppDbContext _context;
    protected DbSet<T> _dbSet;

    public BaseRepository(AppDbContext context)
    {
        _context = context;

        _dbSet = _context.Set<T>();
    }

    public void Create(T entity)
    {
        _dbSet.Add(entity);
    }

    public void Delete(long id)
    {
        var entity = Get(id);

        if (entity != null)
            _dbSet.Remove(entity);
    }

    public IEnumerable<T> GetAll()
    {
        return _dbSet.AsEnumerable();
    }

    public T Get(long id)
    {
        var result = _dbSet.First(f => f.Id == id);

        return result;
    }

    public IEnumerable<T> GetAllByIds(IEnumerable<long> ids)
    {
        return _dbSet.Where(w => ids.Contains(w.Id)).AsEnumerable();
    }

    public void Update(T entity)
    {
        _dbSet.Update(entity);
    }

    public void SaveChange()
    {
        _context.SaveChanges();
    }

    public async Task SaveChangeAsync()
    {
        await _context.SaveChangesAsync();
    }
}