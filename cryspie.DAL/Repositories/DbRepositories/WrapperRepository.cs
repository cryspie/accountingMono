using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.DAL.Repositories.DbRepositories;

public class WrapperRepository:IWrapperRepository
{
    private readonly AppDbContext _appDbContext;
    private IPersonRepository _personRepository;

    public WrapperRepository(AppDbContext appDbContext)
    {
        _appDbContext = appDbContext;
    }
    
    public IPersonRepository PersonRepository 
    {
        get
        {
            if (_personRepository == null)
            {
                _personRepository = new PersonRepository(_appDbContext);
            }
            return _personRepository;
        }       
    }
    
    public void SaveChange()
    {
        _appDbContext.SaveChanges();
    }

    public async Task SaveChangeAsync()
    {
        await _appDbContext.SaveChangesAsync();
    }
}