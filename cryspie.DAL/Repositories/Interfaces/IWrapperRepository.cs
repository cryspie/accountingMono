namespace cryspie.DAL.Repositories.Interfaces;

public interface IWrapperRepository
{
    IPersonRepository PersonRepository { get; }
    Task SaveChangeAsync();
    void SaveChange();
}