﻿namespace cryspie.DAL.Repositories.Interfaces;

public interface IBaseRepository<T>
    where T : class
{
    T Get(long id);
    IEnumerable<T> GetAll();
    IEnumerable<T> GetAllByIds(IEnumerable<long> ids);
    void Create(T entity);
    void Update(T entity);
    void Delete(long id);
    void SaveChange();
    Task SaveChangeAsync();
}
