﻿using cryspie.DAL.Entities;

namespace cryspie.DAL.Repositories.Interfaces;

public interface IKinShipRepository : IBaseRepository<KinShip>
{
}