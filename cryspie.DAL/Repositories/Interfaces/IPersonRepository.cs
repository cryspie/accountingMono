﻿using cryspie.DAL.Entities;

namespace cryspie.DAL.Repositories.Interfaces;

public interface IPersonRepository : IBaseRepository<Person>
{
    Person GetByLogin(string login);
}
