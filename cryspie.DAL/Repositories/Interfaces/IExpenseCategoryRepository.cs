﻿using cryspie.DAL.Entities;

namespace cryspie.DAL.Repositories.Interfaces;

public interface IExpenseCategoryRepository : IBaseRepository<ExpenseCategory>
{
}