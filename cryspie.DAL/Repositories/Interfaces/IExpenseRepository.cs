﻿using cryspie.DAL.Entities;

namespace cryspie.DAL.Repositories.Interfaces;

public interface IExpenseRepository : IBaseRepository<Expense>
{
    IEnumerable<Expense> GetAllByPersonIds(IEnumerable<long> personIds);
    IEnumerable<Expense> GetAllByPersonId(long personId);
}