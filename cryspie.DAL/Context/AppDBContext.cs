﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using cryspie.DAL.Entities;
using System.Reflection.Emit;
using Microsoft.Extensions.Options;

namespace cryspie.DAL
{
    public class AppDbContext : DbContext
    {
        private readonly IConfiguration configuration;

        public AppDbContext(DbContextOptions options, IConfiguration configuration) : base(options)
        {
            this.configuration = configuration;
        }

        protected AppDbContext(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        
        public DbSet<Address> Addresses { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Expense> Expenses { get; set; }
        public DbSet<Family> Families { get; set; }
        public DbSet<ExpenseCategory> ExpenseCategoryies { get; set; }
        public DbSet<KinShip> KinShips { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Address>().ToTable("Address");
            builder.Entity<City>().ToTable("City");
            builder.Entity<Country>().ToTable("Country");
            builder.Entity<Expense>().ToTable("Expense");
            builder.Entity<Family>().ToTable("Family");
            builder.Entity<KinShip>().ToTable("KinShip");
            builder.Entity<Contact>().ToTable("Contact");
            builder.Entity<Person>().ToTable("Person");
            builder.Entity<ExpenseCategory>().ToTable("ExpenseCategory");
            builder.Entity<PhoneNumber>().ToTable("PhoneNumber");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
        }
    }
}
