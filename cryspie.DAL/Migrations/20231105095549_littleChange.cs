﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace cryspie.DAL.Migrations
{
    /// <inheritdoc />
    public partial class littleChange : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_Accountable_AccountableId",
                table: "Expenses");

            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_ExpenseCategoryies_CategoryId",
                table: "Expenses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Expenses",
                table: "Expenses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ExpenseCategoryies",
                table: "ExpenseCategoryies");

            migrationBuilder.RenameTable(
                name: "Expenses",
                newName: "Expense");

            migrationBuilder.RenameTable(
                name: "ExpenseCategoryies",
                newName: "ExpenseCategory");

            migrationBuilder.RenameIndex(
                name: "IX_Expenses_CategoryId",
                table: "Expense",
                newName: "IX_Expense_CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Expenses_AccountableId",
                table: "Expense",
                newName: "IX_Expense_AccountableId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Expense",
                table: "Expense",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExpenseCategory",
                table: "ExpenseCategory",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Expense_Accountable_AccountableId",
                table: "Expense",
                column: "AccountableId",
                principalTable: "Accountable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Expense_ExpenseCategory_CategoryId",
                table: "Expense",
                column: "CategoryId",
                principalTable: "ExpenseCategory",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expense_Accountable_AccountableId",
                table: "Expense");

            migrationBuilder.DropForeignKey(
                name: "FK_Expense_ExpenseCategory_CategoryId",
                table: "Expense");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ExpenseCategory",
                table: "ExpenseCategory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Expense",
                table: "Expense");

            migrationBuilder.RenameTable(
                name: "ExpenseCategory",
                newName: "ExpenseCategoryies");

            migrationBuilder.RenameTable(
                name: "Expense",
                newName: "Expenses");

            migrationBuilder.RenameIndex(
                name: "IX_Expense_CategoryId",
                table: "Expenses",
                newName: "IX_Expenses_CategoryId");

            migrationBuilder.RenameIndex(
                name: "IX_Expense_AccountableId",
                table: "Expenses",
                newName: "IX_Expenses_AccountableId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ExpenseCategoryies",
                table: "ExpenseCategoryies",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Expenses",
                table: "Expenses",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_Accountable_AccountableId",
                table: "Expenses",
                column: "AccountableId",
                principalTable: "Accountable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_ExpenseCategoryies_CategoryId",
                table: "Expenses",
                column: "CategoryId",
                principalTable: "ExpenseCategoryies",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
