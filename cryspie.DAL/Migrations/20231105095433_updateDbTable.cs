﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace cryspie.DAL.Migrations
{
    /// <inheritdoc />
    public partial class updateDbTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accountables_Contacts_ContactId",
                table: "Accountables");

            migrationBuilder.DropForeignKey(
                name: "FK_Accountables_Family_FamilyId",
                table: "Accountables");

            migrationBuilder.DropForeignKey(
                name: "FK_Accountables_PhoneNumbers_PhoneNumberId",
                table: "Accountables");

            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_Address_AddressId",
                table: "Contacts");

            migrationBuilder.DropForeignKey(
                name: "FK_Contacts_PhoneNumbers_PhoneNumberId",
                table: "Contacts");

            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_Accountables_AccountableId",
                table: "Expenses");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PhoneNumbers",
                table: "PhoneNumbers");

            migrationBuilder.DropPrimaryKey(
                name: "PK_KinShips",
                table: "KinShips");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contacts",
                table: "Contacts");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accountables",
                table: "Accountables");

            migrationBuilder.DropIndex(
                name: "IX_Accountables_ContactId",
                table: "Accountables");

            migrationBuilder.DropIndex(
                name: "IX_Accountables_FamilyId",
                table: "Accountables");

            migrationBuilder.DropColumn(
                name: "ContactId",
                table: "Accountables");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Accountables");

            migrationBuilder.DropColumn(
                name: "FamilyId",
                table: "Accountables");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Accountables");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "Accountables");

            migrationBuilder.RenameTable(
                name: "PhoneNumbers",
                newName: "PhoneNumber");

            migrationBuilder.RenameTable(
                name: "KinShips",
                newName: "KinShip");

            migrationBuilder.RenameTable(
                name: "Contacts",
                newName: "Contact");

            migrationBuilder.RenameTable(
                name: "Accountables",
                newName: "Accountable");

            migrationBuilder.RenameIndex(
                name: "IX_Contacts_PhoneNumberId",
                table: "Contact",
                newName: "IX_Contact_PhoneNumberId");

            migrationBuilder.RenameIndex(
                name: "IX_Contacts_AddressId",
                table: "Contact",
                newName: "IX_Contact_AddressId");

            migrationBuilder.RenameIndex(
                name: "IX_Accountables_PhoneNumberId",
                table: "Accountable",
                newName: "IX_Accountable_PhoneNumberId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PhoneNumber",
                table: "PhoneNumber",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_KinShip",
                table: "KinShip",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contact",
                table: "Contact",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accountable",
                table: "Accountable",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Person",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false),
                    FirstName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: true),
                    ContactId = table.Column<long>(type: "bigint", nullable: false),
                    FamilyId = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Person", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Person_Accountable_Id",
                        column: x => x.Id,
                        principalTable: "Accountable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Person_Contact_ContactId",
                        column: x => x.ContactId,
                        principalTable: "Contact",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Person_Family_FamilyId",
                        column: x => x.FamilyId,
                        principalTable: "Family",
                        principalColumn: "Id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Person_ContactId",
                table: "Person",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Person_FamilyId",
                table: "Person",
                column: "FamilyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accountable_PhoneNumber_PhoneNumberId",
                table: "Accountable",
                column: "PhoneNumberId",
                principalTable: "PhoneNumber",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_Address_AddressId",
                table: "Contact",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_PhoneNumber_PhoneNumberId",
                table: "Contact",
                column: "PhoneNumberId",
                principalTable: "PhoneNumber",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_Accountable_AccountableId",
                table: "Expenses",
                column: "AccountableId",
                principalTable: "Accountable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Accountable_PhoneNumber_PhoneNumberId",
                table: "Accountable");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact_Address_AddressId",
                table: "Contact");

            migrationBuilder.DropForeignKey(
                name: "FK_Contact_PhoneNumber_PhoneNumberId",
                table: "Contact");

            migrationBuilder.DropForeignKey(
                name: "FK_Expenses_Accountable_AccountableId",
                table: "Expenses");

            migrationBuilder.DropTable(
                name: "Person");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PhoneNumber",
                table: "PhoneNumber");

            migrationBuilder.DropPrimaryKey(
                name: "PK_KinShip",
                table: "KinShip");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Contact",
                table: "Contact");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Accountable",
                table: "Accountable");

            migrationBuilder.RenameTable(
                name: "PhoneNumber",
                newName: "PhoneNumbers");

            migrationBuilder.RenameTable(
                name: "KinShip",
                newName: "KinShips");

            migrationBuilder.RenameTable(
                name: "Contact",
                newName: "Contacts");

            migrationBuilder.RenameTable(
                name: "Accountable",
                newName: "Accountables");

            migrationBuilder.RenameIndex(
                name: "IX_Contact_PhoneNumberId",
                table: "Contacts",
                newName: "IX_Contacts_PhoneNumberId");

            migrationBuilder.RenameIndex(
                name: "IX_Contact_AddressId",
                table: "Contacts",
                newName: "IX_Contacts_AddressId");

            migrationBuilder.RenameIndex(
                name: "IX_Accountable_PhoneNumberId",
                table: "Accountables",
                newName: "IX_Accountables_PhoneNumberId");

            migrationBuilder.AddColumn<long>(
                name: "ContactId",
                table: "Accountables",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Accountables",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "FamilyId",
                table: "Accountables",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Accountables",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "Accountables",
                type: "text",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PhoneNumbers",
                table: "PhoneNumbers",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_KinShips",
                table: "KinShips",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Contacts",
                table: "Contacts",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Accountables",
                table: "Accountables",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Accountables_ContactId",
                table: "Accountables",
                column: "ContactId");

            migrationBuilder.CreateIndex(
                name: "IX_Accountables_FamilyId",
                table: "Accountables",
                column: "FamilyId");

            migrationBuilder.AddForeignKey(
                name: "FK_Accountables_Contacts_ContactId",
                table: "Accountables",
                column: "ContactId",
                principalTable: "Contacts",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Accountables_Family_FamilyId",
                table: "Accountables",
                column: "FamilyId",
                principalTable: "Family",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Accountables_PhoneNumbers_PhoneNumberId",
                table: "Accountables",
                column: "PhoneNumberId",
                principalTable: "PhoneNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_Address_AddressId",
                table: "Contacts",
                column: "AddressId",
                principalTable: "Address",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Contacts_PhoneNumbers_PhoneNumberId",
                table: "Contacts",
                column: "PhoneNumberId",
                principalTable: "PhoneNumbers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Expenses_Accountables_AccountableId",
                table: "Expenses",
                column: "AccountableId",
                principalTable: "Accountables",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
