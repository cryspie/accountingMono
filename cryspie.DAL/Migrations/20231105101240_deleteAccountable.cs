﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace cryspie.DAL.Migrations
{
    /// <inheritdoc />
    public partial class deleteAccountable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expense_Accountable_AccountableId",
                table: "Expense");

            migrationBuilder.DropForeignKey(
                name: "FK_Person_Accountable_Id",
                table: "Person");

            migrationBuilder.DropTable(
                name: "Accountable");

            migrationBuilder.RenameColumn(
                name: "AccountableId",
                table: "Expense",
                newName: "PersonId");

            migrationBuilder.RenameIndex(
                name: "IX_Expense_AccountableId",
                table: "Expense",
                newName: "IX_Expense_PersonId");

            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "Person",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Person",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "KinShipId",
                table: "Person",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<string>(
                name: "Login",
                table: "Person",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Person",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Password",
                table: "Person",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Person_KinShipId",
                table: "Person",
                column: "KinShipId");

            migrationBuilder.AddForeignKey(
                name: "FK_Expense_Person_PersonId",
                table: "Expense",
                column: "PersonId",
                principalTable: "Person",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Person_KinShip_KinShipId",
                table: "Person",
                column: "KinShipId",
                principalTable: "KinShip",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Expense_Person_PersonId",
                table: "Expense");

            migrationBuilder.DropForeignKey(
                name: "FK_Person_KinShip_KinShipId",
                table: "Person");

            migrationBuilder.DropIndex(
                name: "IX_Person_KinShipId",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "KinShipId",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "Login",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "Name",
                table: "Person");

            migrationBuilder.DropColumn(
                name: "Password",
                table: "Person");

            migrationBuilder.RenameColumn(
                name: "PersonId",
                table: "Expense",
                newName: "AccountableId");

            migrationBuilder.RenameIndex(
                name: "IX_Expense_PersonId",
                table: "Expense",
                newName: "IX_Expense_AccountableId");

            migrationBuilder.AlterColumn<long>(
                name: "Id",
                table: "Person",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint")
                .OldAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.CreateTable(
                name: "Accountable",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    PhoneNumberId = table.Column<long>(type: "bigint", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: true),
                    Login = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accountable", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Accountable_PhoneNumber_PhoneNumberId",
                        column: x => x.PhoneNumberId,
                        principalTable: "PhoneNumber",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accountable_PhoneNumberId",
                table: "Accountable",
                column: "PhoneNumberId");

            migrationBuilder.AddForeignKey(
                name: "FK_Expense_Accountable_AccountableId",
                table: "Expense",
                column: "AccountableId",
                principalTable: "Accountable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Person_Accountable_Id",
                table: "Person",
                column: "Id",
                principalTable: "Accountable",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
