﻿using cryspie.BLL.EntitiesDTO.Expense;
using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace cryspie.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class FamilyController : Controller
{
    private readonly IFamilyService _familyService;
    private readonly IExpenseServise _expenseServise;

    public FamilyController(
        IFamilyService familyService,
        IExpenseServise expenseServise)
    {
        _familyService = familyService;
        _expenseServise = expenseServise;
    }

    [HttpGet]
    public Family Get(long id)
    {
        return _familyService.Get(id);
    }

    //[HttpGet]
    //public IEnumerable<ExpenseDTO> GetExpensesByPersons(IEnumerable<Person> persons)
    //{
    //    return _expenseServise.GetExpensesByPersonIds(persons.Select(x => x.Id));
    //}

    [HttpGet("[action]")]
    public double GetSumExpensesByFamily(IEnumerable<Person> persons)
    {
        return _expenseServise.GetSumExpensesByPersonIds(persons.Select(x => x.Id));
    }

    [HttpPut]
    public void Put(Family family)
    {
        _familyService.Create(family);
    }

    [HttpPost]
    public void Post(Family family)
    {
        _familyService.Update(family);
    }

    [HttpDelete]
    public void Delete(long id)
    {
        _familyService.Delete(id);
    }
}