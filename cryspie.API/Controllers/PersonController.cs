﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace cryspie.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonController : Controller
    {
        private readonly IPersonService _personService;

        public PersonController(IPersonService personService)
        {
            _personService = personService;
        }

        [HttpGet]
        public Person Get(long id)
        {
            return _personService.Get(id);
        }

        [HttpGet("[action]")]
        public Person GetUserByLogin(string login)
        {
            return _personService.GetByLogin(login);
        }

        [HttpPut]
        public void Put(Person user)
        {
            _personService.Create(user);
        }

        [HttpPost]
        public void Post(Person user)
        {
            _personService.Update(user);
        }

        [HttpDelete]
        public void Delete(long id)
        {
            _personService.Delete(id);
        }

    }
}
