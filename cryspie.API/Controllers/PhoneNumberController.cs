﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace cryspie.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PhoneNumberController : Controller
{
    private readonly IPhoneNumberService _phoneNumberService;

    public PhoneNumberController(IPhoneNumberService phoneNumberService)
    {
        _phoneNumberService = phoneNumberService;
    }

    [HttpGet]
    public PhoneNumber Get(long id)
    {
        return _phoneNumberService.Get(id);
    }

    [HttpGet("[action]")]
    public List<PhoneNumber> AllPhoneNumbers()
    {
        return _phoneNumberService.GetAll();
    }
    
    [HttpPut]
    public void Put(PhoneNumber phoneNumber)
    {
        _phoneNumberService.Create(phoneNumber);
    }

    [HttpPost]
    public void Post(PhoneNumber phoneNumber)
    {
        _phoneNumberService.Update(phoneNumber);
    }

    [HttpDelete]
    public void Delete(long id)
    {
        _phoneNumberService.Delete(id);
    }
}