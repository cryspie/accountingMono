﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace cryspie.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CountryController : Controller
{
    private readonly ICountryService _countryService;

    public CountryController(ICountryService countryService)
    {
        _countryService = countryService;
    }

    [HttpGet]
    public Country Get(long id)
    {
        return _countryService.Get(id);
    }

    [HttpGet("[action]")]
    public List<Country> AllCountrys()
    {
        return _countryService.GetAll();
    }
    
    [HttpPut]
    public void Put(Country country)
    {
        _countryService.Create(country);
    }

    [HttpPost]
    public void Post(Country country)
    {
        _countryService.Update(country);
    }

    [HttpDelete]
    public void Delete(long id)
    {
        _countryService.Delete(id);
    }
}