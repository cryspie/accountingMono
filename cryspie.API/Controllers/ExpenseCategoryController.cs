﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace cryspie.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ExpenseCategoryController : Controller
{
    private readonly IExpenseCategoryService _expenseCategoryService;

    public ExpenseCategoryController(IExpenseCategoryService expenseCategoryService)
    {
        _expenseCategoryService = expenseCategoryService;
    }

    [HttpGet]
    public ExpenseCategory Get(long id)
    {
        return _expenseCategoryService.Get(id);
    }

    [HttpGet("[action]")]
    public List<ExpenseCategory> AllExpenseCategories()
    {
        return _expenseCategoryService.GetAll();
    }
    
    [HttpPut]
    public void Put(ExpenseCategory expenseCategory)
    {
        _expenseCategoryService.Create(expenseCategory);
    }

    [HttpPost]
    public void Post(ExpenseCategory expenseCategory)
    {
        _expenseCategoryService.Update(expenseCategory);
    }

    [HttpDelete]
    public void Delete(long id)
    {
        _expenseCategoryService.Delete(id);
    }
}