﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace cryspie.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class CityController : Controller
{
    private readonly ICityServise _cityService;

    public CityController(ICityServise cityServiceService)
    {
        _cityService = cityServiceService;
    }

    [HttpGet]
    public City Get(long id)
    {
        return _cityService.Get(id);
    }

    [HttpGet("[action]")]
    public List<City> AllCitys()
    {
        return _cityService.GetAll();
    }
    
    [HttpPut]
    public void Put(City city)
    {
        _cityService.Create(city);
    }

    [HttpPost]
    public void Post(City city)
    {
        _cityService.Update(city);
    }

    [HttpDelete]
    public void Delete(long id)
    {
        _cityService.Delete(id);
    }
}