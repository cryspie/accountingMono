﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace cryspie.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ExpenseController : Controller
{
    private readonly IExpenseServise _expenseService;

    public ExpenseController(IExpenseServise expenseService)
    {
        _expenseService = expenseService;
    }

    [HttpGet]
    public Expense Get(long id)
    {
        return _expenseService.Get(id);
    }

    [HttpGet("[action]")]
    public List<Expense> AllExpenses()
    {
        return _expenseService.GetAll();
    }

    [HttpPut]
    public void Put(Expense expense)
    {
        _expenseService.Create(expense);
    }

    [HttpPost]
    public void Post(Expense expense)
    {
        _expenseService.Update(expense);
    }

    [HttpDelete]
    public void Delete(long id)
    {
        _expenseService.Delete(id);
    }
}