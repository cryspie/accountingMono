﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace cryspie.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AddressController : Controller
{
    private readonly IAddressService _addresService;

    public AddressController(IAddressService addressService)
    {
        _addresService = addressService;
    }

    [HttpGet]
    public Address Get(long id)
    {
        return _addresService.Get(id);
    }

    [HttpGet("[action]")]
    public List<Address> AllAddresses()
    {
        return _addresService.GetAll();
    }
    
    [HttpPut]
    public void Put(Address address)
    {
        _addresService.Create(address);
    }

    [HttpPost]
    public void Post(Address address)
    {
        _addresService.Update(address);
    }

    [HttpDelete]
    public void Delete(long id)
    {
        _addresService.Delete(id);
    }
}