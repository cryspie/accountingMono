﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using Microsoft.AspNetCore.Mvc;

namespace cryspie.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class KinShipController : Controller
{
    private readonly IKinShipService _kinShipService;

    public KinShipController(IKinShipService kinShipService)
    {
        _kinShipService = kinShipService;
    }

    [HttpGet]
    public KinShip Get(long id)
    {
        return _kinShipService.Get(id);
    }

    [HttpGet("[action]")]
    public List<KinShip> AllKinShips()
    {
        return _kinShipService.GetAll();
    }
    
    [HttpPut]
    public void Put(KinShip kinShip)
    {
        _kinShipService.Create(kinShip);
    }

    [HttpPost]
    public void Post(KinShip kinShip)
    {
        _kinShipService.Update(kinShip);
    }

    [HttpDelete]
    public void Delete(long id)
    {
        _kinShipService.Delete(id);
    }
}