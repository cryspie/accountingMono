﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public class AddresService : ServiceBase<IAddresRepository, Address>, IAddressService
{
    public AddresService(IAddresRepository repository) : base(repository)
    {
    }
}