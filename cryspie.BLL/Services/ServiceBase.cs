﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public abstract class ServiceBase<R, T> : IServiceBase<T>
    where R : IBaseRepository<T>
    where T : EntityBase
{
    internal R _repository;

    protected ServiceBase(R repository)
    {
        _repository = repository;
    }

    public virtual void Create(T entity)
    {
        _repository.Create(entity);

        _repository.SaveChange();
    }

    public virtual void Delete(long id)
    {
        _repository.Delete(id);
            
        _repository.SaveChange();
    }

    public virtual T Get(long id)
    {
        return _repository.Get(id);
    }

    public virtual List<T> GetAllByIds(IEnumerable<long> ids)
    {
        var result = _repository.GetAllByIds(ids);

        return result.ToList();
    }

    public virtual List<T> GetAll()
    {
        var result = _repository.GetAll();

        return result.ToList();
    }

    public virtual void Update(T entity)
    {
        _repository.Update(entity);

        _repository.SaveChange();
    }
}