﻿using cryspie.DAL.Entities;

namespace cryspie.BLL.Services.Contracts;

public interface IKinShipService : IServiceBase<KinShip>
{
}