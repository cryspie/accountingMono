﻿using cryspie.DAL.Entities;

namespace cryspie.BLL.Services.Contracts;

public interface ICityServise : IServiceBase<City>
{
}