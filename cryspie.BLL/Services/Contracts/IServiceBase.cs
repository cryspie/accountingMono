﻿using cryspie.DAL.Entities;

namespace cryspie.BLL.Services.Contracts;

public interface IServiceBase<T> where T : EntityBase
{
    void Create(T entity);
    void Delete(long id);
    List<T> GetAllByIds(IEnumerable<long> ids);
    T Get(long id);
    List<T> GetAll();
    void Update(T entity);
}