﻿using cryspie.DAL.Entities;

namespace cryspie.BLL.Services.Contracts;

public interface IAddressService : IServiceBase<Address>
{
}