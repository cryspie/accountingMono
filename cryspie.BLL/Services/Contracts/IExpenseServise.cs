﻿using cryspie.BLL.EntitiesDTO.Expense;
using cryspie.DAL.Entities;

namespace cryspie.BLL.Services.Contracts;

public interface IExpenseServise : IServiceBase<Expense>
{
    IEnumerable<Expense> GetExpensesByPerson(long id);
    IEnumerable<ExpenseDTO> GetExpensesByPersonIds(IEnumerable<long> select);
    double GetSumExpensesByPersonIds(IEnumerable<long> select);
}