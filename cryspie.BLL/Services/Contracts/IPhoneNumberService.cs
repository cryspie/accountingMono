﻿using cryspie.DAL.Entities;

namespace cryspie.BLL.Services.Contracts;

public interface IPhoneNumberService : IServiceBase<PhoneNumber>
{
}