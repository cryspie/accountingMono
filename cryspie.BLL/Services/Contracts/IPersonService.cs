﻿using cryspie.DAL.Entities;

namespace cryspie.BLL.Services.Contracts;

public interface IPersonService : IServiceBase<Person>
{
    Person GetByLogin(string login);
    // IEnumerable<Person> GetPersonsByLastName(string lastName);
}