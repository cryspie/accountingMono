﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public class FamilyService : ServiceBase<IFamilyRepository, Family>, IFamilyService
{
    public FamilyService(IFamilyRepository repository) : base(repository)
    {
    }
}