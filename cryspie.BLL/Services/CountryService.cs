﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public class CountryService : ServiceBase<ICountryRepository, Country>, ICountryService
{
    public CountryService(ICountryRepository repository) : base(repository)
    {
    }
}