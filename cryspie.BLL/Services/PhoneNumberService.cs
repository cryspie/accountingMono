﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public class PhoneNumberService : ServiceBase<IPhoneNumberRepository, PhoneNumber>, IPhoneNumberService
{
    public PhoneNumberService(IPhoneNumberRepository repository) : base(repository)
    {
    }
}