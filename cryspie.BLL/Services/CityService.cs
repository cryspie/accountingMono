﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public class CityService : ServiceBase<ICityRepository, City>, ICityServise
{
    public CityService(ICityRepository repository) : base(repository)
    {
    }
}