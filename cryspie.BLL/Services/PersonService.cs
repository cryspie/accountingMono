﻿using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;
using cryspie.BLL.Services.Contracts;

namespace cryspie.BLL.Services;

public class PersonService : ServiceBase<IPersonRepository, Person>, IPersonService
{
    public PersonService(IPersonRepository repository) : base(repository)
    {
    }

    public Person GetByLogin(string login)
    {
        return _repository.GetByLogin(login);
    }
}