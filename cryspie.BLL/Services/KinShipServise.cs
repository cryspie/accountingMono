﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public class KinShipServise : ServiceBase<IKinShipRepository, KinShip>, IKinShipService
{
    public KinShipServise(IKinShipRepository repository) : base(repository)
    {
    }
}