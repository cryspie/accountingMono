﻿using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public class ExpenseCategoryService : ServiceBase<IExpenseCategoryRepository, ExpenseCategory>, IExpenseCategoryService
{
    public ExpenseCategoryService(IExpenseCategoryRepository repository) : base(repository)
    {
    }
}