﻿using cryspie.BLL.EntitiesDTO.Expense;
using cryspie.BLL.Services.Contracts;
using cryspie.DAL.Entities;
using cryspie.DAL.Repositories.Interfaces;

namespace cryspie.BLL.Services;

public class ExpenseServise : ServiceBase<IExpenseRepository, Expense>, IExpenseServise
{
    public ExpenseServise(IExpenseRepository repository) : base(repository)
    {
    }

    public IEnumerable<Expense> GetExpensesByPerson(long id)
    {
        return _repository.GetAllByPersonId(id);
    }

    public IEnumerable<ExpenseDTO> GetExpensesByPersonIds(IEnumerable<long> personIds)
    {
        return _repository.GetAllByPersonIds(personIds).Select(x => new ExpenseDTO()
        {
            Cost = x.Cost,
            CategoryName = x.Category.Name,
            PersonName = x.Person.Name,
            PersonId = x.Person.Id
        });
    }

    public double GetSumExpensesByPersonIds(IEnumerable<long> personIds)
    {
        return _repository.GetAllByPersonIds(personIds).Sum(x=>x.Cost);
    }
}