using cryspie.BLL.Services;
using cryspie.BLL.Services.Contracts;
using Microsoft.Extensions.DependencyInjection;

namespace cryspie.BLL.Extensions;

public static class ServiceExtension
{
    public static void ConfigurationServicesBLL(this IServiceCollection services)
    {
        services.AddScoped<IPersonService, PersonService>();
        services.AddScoped<IAddressService, AddresService>();
        services.AddScoped<ICityServise, CityService>();
        services.AddScoped<ICountryService, CountryService>();
        services.AddScoped<IExpenseServise, ExpenseServise>();
        services.AddScoped<IExpenseCategoryService, ExpenseCategoryService>();
        services.AddScoped<IFamilyService, FamilyService>();
        services.AddScoped<IKinShipService, KinShipServise>();
        services.AddScoped<IPhoneNumberService, PhoneNumberService>();
    }

    // public static void ConfigurationAutoMapper(this IServiceCollection services)
    // {
    //     services.AddAutoMapper(typeof(MappingProfile));
    // }
}