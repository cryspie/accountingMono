namespace cryspie.BLL.EntitiesDTO.Expense;

public class ExpenseDTO
{
    public double Cost { get; set; }
    public string CategoryName { get; set; }
    public string PersonName { get; set; }
    public double PersonId { get; set; }
}